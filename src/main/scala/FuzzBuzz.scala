object FuzzBuzz extends App {
  def fuzzBuzz(input: Seq[Int]): Seq[String] = {
    return input.map(replaceElement)
  }

  override def main(args: Array[String]): Unit = {
    println(this.fuzzBuzz(Seq(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15, -16)))
  }

  def replaceElement(x: Int): String = {
    if (x % 15 == 0){
      return "Fizz Bizz"
    } else if (x % 5 == 0){
      return "Buzz"
    } else if(x % 3 == 0){
      return "Fizz"
    } else {
      return x.toString()
    }
  }
}
