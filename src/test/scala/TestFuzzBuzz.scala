import org.scalatest.FunSuite


class TestFuzzBuzz extends FunSuite {
  test("FuzzBuzz.FuzzBuzz") {
    assert(FuzzBuzz.fuzzBuzz(Seq(1)) === Seq("1","2","3"))
  }
}
